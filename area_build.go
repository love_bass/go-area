package area

import "sort"

// buildIndex 构建一个索引，以便快速查询
func buildIndex(areas []Area) ([]Area, map[uint64][]Area, map[uint64][]Area) {
	provinces := make([]Area, 0)
	provinceToCity := make(map[uint64][]Area)
	cityToCounty := make(map[uint64][]Area)

	for _, area := range areas {
		if area.Level == 1 { // 省级
			provinces = append(provinces, area)
		} else if area.Level == 2 { // 市级
			provinceToCity[area.UpperID] = append(provinceToCity[area.UpperID], area)
		} else if area.Level == 3 { // 县级
			cityToCounty[area.UpperID] = append(cityToCounty[area.UpperID], area)
		}
	}
	return provinces, provinceToCity, cityToCounty
}

// buildReverseIndex 构建反向索引，以便通过当前区域的ID快速查询到上级区域
func buildReverseIndex(areas []Area) map[uint64]Area {
	reverseIndex := make(map[uint64]Area)
	for _, area := range areas {
		reverseIndex[area.ID] = area
	}
	return reverseIndex
}

// sortAreasByID 对地区按 ID 进行排序
func sortAreasByID(areas []Area) {
	sort.Slice(areas, func(i, j int) bool {
		return areas[i].ID < areas[j].ID
	})
}
