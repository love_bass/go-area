package area

import (
	"encoding/json"
	"strings"
)

type Area struct {
	ID      uint64 `json:"id"`
	Title   string `json:"title"`
	Level   uint8  `json:"level"`
	UpperID uint64 `json:"upperID"`
}

var areas []Area
var provinces []Area
var provinceToCity map[uint64][]Area
var cityToCounty map[uint64][]Area
var reverseIndex map[uint64]Area

func init() {
	// 将json字符串转为Area结构体
	var err error
	areas, err = parseArea(jsonStr)
	if err != nil {
		panic(err)
	}
	// 构建索引
	provinces, provinceToCity, cityToCounty = buildIndex(areas)
	reverseIndex = buildReverseIndex(areas)
	// 省排序
	sortAreasByID(provinces)
	// 加载IP归属地数据
	initRegion()
}

// parseArea 将json字符串转为Area结构体
func parseArea(jsonStr string) ([]Area, error) {
	var areas []Area
	err := json.Unmarshal([]byte(jsonStr), &areas)
	if err != nil {
		return nil, err
	}
	return areas, nil
}

func GetProvinces() []Area {
	return provinces
}

func GetCitiesByProvinceID(provinceID uint64) []Area {
	cities := provinceToCity[provinceID]
	sortAreasByID(cities) // 对城市进行排序
	return cities
}

// GetCountiesByCityID 获取指定市ID的县列表
func GetCountiesByCityID(cityID uint64) []Area {
	counties := cityToCounty[cityID]
	sortAreasByID(counties) // 对县进行排序
	return counties
}

// GetUpperArea 获取指定区域ID的上级区域数据
func GetUpperArea(currentID uint64) (Area, bool) {
	if area, exists := reverseIndex[currentID]; exists {
		// 如果本身等级为1，则表明是省级的，无上级区域
		if area.Level == 1 {
			return Area{}, false
		}
		if upperArea, exists := reverseIndex[area.UpperID]; exists {
			return upperArea, true
		}
	}
	return Area{}, false
}

// SearchProvinceByTitle 根据省名称搜索地区ID
func SearchProvinceByTitle(keyword string) (Area, bool) {
	// 遍历查找
	for _, province := range provinces {
		if strings.Contains(province.Title, keyword) {
			return province, true
		}
	}
	return Area{}, false
}

func SearchProvincesByTitle(keyword string) []Area {
	var results []Area
	// 遍历查找
	for _, province := range provinces {
		if strings.Contains(province.Title, keyword) {
			results = append(results, province)
		}
	}
	return results
}

// IsExistingCityById 根据市ID判断市是否存在
func IsExistingCityById(cityId uint64) (Area, bool) {
	// 只需要判断有没有上级省就行
	province, exists := GetUpperArea(cityId)
	if !exists {
		return Area{}, false
	}
	// 再通过省ID获取市列表
	cities := provinceToCity[province.ID]
	for _, city := range cities {
		if city.ID == cityId {
			return city, true
		}
	}
	return Area{}, false
}

// SearchCityByTitle 根据市名称搜索指定ID的省下的市
func SearchCityByTitle(provinceId uint64, keyword string) (Area, bool) {
	// 获取市级列表
	cities := provinceToCity[provinceId]
	// 遍历查找
	for _, city := range cities {
		if strings.Contains(city.Title, keyword) {
			return city, true
		}
	}
	return Area{}, false
}

// SearchCitiesByTitle 根据市名称搜索指定ID的省下的市
func SearchCitiesByTitle(provinceId uint64, keyword string) []Area {
	var results []Area
	// 获取市级列表
	cities := provinceToCity[provinceId]
	// 遍历查找
	for _, city := range cities {
		if strings.Contains(city.Title, keyword) {
			results = append(results, city)
		}
	}
	return results
}
